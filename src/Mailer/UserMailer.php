<?php
namespace App\Mailer;

use Cake\Mailer\Mailer;

class UserMailer extends Mailer
{
    public function welcome($user)
    {
        $this
            ->to($user->email)
            ->subject(sprintf('Welcome %s', $user->name))
            ->template('welcome_mail') // By default template with same name as method name is used.
            ->layout('custom');
    }

    public function userActivation($user, $a_email_params)
    {
        $this
            ->from(['no-reply@trusted-freelancer.com' => 'BUM Cakev3'])
            ->to($user->email)
            ->emailFormat('html')
            ->subject( 'BUM Cakev3 - User - Register Activation' )
            ->template('user_activation', 'user')
            ->viewVars(['o_user' => $user, 'a_email_params' => $a_email_params]);
    }

    public function forgotPassword($user, $a_email_params)
    {
        $this
            ->from(['no-reply@trusted-freelancer.com' => 'BUM Cakev3'])
            ->to($user->email)
            ->emailFormat('html')
            ->subject( 'BUM Cakev3 - User - Reset Password Request' )
            ->template('user_reset_password', 'user')
            ->viewVars(['o_user' => $user, 'a_email_params' => $a_email_params]);
    }
}