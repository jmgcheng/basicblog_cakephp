<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Cookie', ['expiry' => '1 day']);
        $this->loadComponent('Auth', [
            'authorize'=> 'Controller',
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'username',
                        'password' => 'password'
                    ]
                ]
            ],
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'unauthorizedRedirect' => $this->referer()
        ]);
        $this->loadComponent('User');

        // Allow the display action so our pages controller
        // continues to work.
        $this->Auth->allow(['display']);        
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }


    /*
        will auto trigger if page visiting needs to check if user isAuthorized
    */
    public function isAuthorized($user)
    {
        //get current controller and action
        $s_action = '';
        $s_controller = '';
        if( isset($this->request->params['action']) && !empty($this->request->params['action']) )
        {
            $s_action = $this->request->params['action'];
        }
        if( isset($this->request->params['controller']) && !empty($this->request->params['controller']) )
        {
            $s_controller = strtolower($this->request->params['controller']);
        }

        //check user session roles if authrized to visit current page
        /*if( isset($_SESSION['Auth']['User']['a_roles_flat']) && !empty($_SESSION['Auth']['User']['a_roles_flat']) )
        {
            $a_user_session_roles = $_SESSION['Auth']['User']['a_roles_flat'];
            if(in_array('admin', $a_user_session_roles)) 
            {
                return true;
            }
        }*/
        if( $this->User->hasRoles(['admin']) )
        {
            return true;
        }

        return false;
    }


    /*
        Called during the Controller.initialize event which occurs before every action in the controller. 
        It’s a handy place to check for an active session or inspect user permissions.
    */
    public function beforeFilter(Event $event) 
    {
        //autologin
        if (!$this->Auth->user('id') && $this->Cookie->read('sitename_autologin'))
        {
            $s_cookie_key = $this->Cookie->read('sitename_autologin');

            $o_user = $this->Users->find();
            $o_user->contain(['StatusUsers']);
            $o_user->where(['unique_key' => $s_cookie_key]);
            $o_user->where(['StatusUsers.name' => 'activated']);
            $o_user->hydrate(false);
            $o_user_result = $o_user->first();
            
            if( isset($o_user_result) && !empty($o_user_result) )
            {
                $this->Auth->setUser($o_user_result);
                return $this->redirect($this->Auth->redirectUrl());
            }

        }

        //add user session details if needed
        if(     isset($_SESSION['Auth']['User']) && !empty($_SESSION['Auth']['User']) 
            && !empty($_SESSION['Auth']['User']['id']) 
        )
        {
            // get users with their status_users and roles
            $this->loadModel('Users');
            $i_user_id = $this->Auth->user('id'); //or $_SESSION['Auth']['User']['id']
            $a_query_result = [];
            $a_user_roles = [];
            if( isset($i_user_id) && !empty($i_user_id) && $i_user_id > 0 )
            {
                $a_query_result = $this->Users->find()
                            //->select(['id', 'username'])
                            ->where(['users.id' => $i_user_id])
                            ->contain(['StatusUsers', 'Roles'])
                            ->hydrate(false);
                $a_query_result = $a_query_result->first();
                if( isset($a_query_result) && !empty($a_query_result) )
                {
                    //add a_roles_flat to user session
                    if( isset($a_query_result['roles']) && !empty($a_query_result['roles']) )   
                    {
                        $a_roles_flat = array();
                        foreach( $a_query_result['roles'] AS $a_role_detail )   
                        {
                            array_push( $a_roles_flat, strtolower($a_role_detail['name']) );
                        }
                        $a_query_result['a_roles_flat'] = $a_roles_flat;
                    }
                    if( isset( $a_query_result['a_roles_flat'] ) && !empty( $a_query_result['a_roles_flat'] ) )
                    {
                        $_SESSION['Auth']['User']['a_roles_flat'] = $a_query_result['a_roles_flat'];
                    }
                }
            }

            //set for view
            $this->set('a_user_session', $_SESSION['Auth']['User']);
        }

    }
}
