<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * StatusArticles Controller
 *
 * @property \App\Model\Table\StatusArticlesTable $StatusArticles
 */
class StatusArticlesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $statusArticles = $this->paginate($this->StatusArticles);

        $this->set(compact('statusArticles'));
        $this->set('_serialize', ['statusArticles']);
    }

    /**
     * View method
     *
     * @param string|null $id Status Article id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $statusArticle = $this->StatusArticles->get($id, [
            'contain' => ['Articles']
        ]);

        $this->set('statusArticle', $statusArticle);
        $this->set('_serialize', ['statusArticle']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $statusArticle = $this->StatusArticles->newEntity();
        if ($this->request->is('post')) {
            $statusArticle = $this->StatusArticles->patchEntity($statusArticle, $this->request->data);
            if ($this->StatusArticles->save($statusArticle)) {
                $this->Flash->success(__('The status article has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The status article could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('statusArticle'));
        $this->set('_serialize', ['statusArticle']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Status Article id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $statusArticle = $this->StatusArticles->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $statusArticle = $this->StatusArticles->patchEntity($statusArticle, $this->request->data);
            if ($this->StatusArticles->save($statusArticle)) {
                $this->Flash->success(__('The status article has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The status article could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('statusArticle'));
        $this->set('_serialize', ['statusArticle']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Status Article id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $statusArticle = $this->StatusArticles->get($id);
        if ($this->StatusArticles->delete($statusArticle)) {
            $this->Flash->success(__('The status article has been deleted.'));
        } else {
            $this->Flash->error(__('The status article could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
