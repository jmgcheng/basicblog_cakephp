<nav class="large-3 medium-4 columns" id="actions-sidebar">
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create() ?>
    <fieldset>
        <legend><?= __('Reset Account Password') ?></legend>
        <?php
            echo $this->Form->input('email', ['default' => $email, 'readonly' => 'readonly', 'required' => 'required']);
            echo $this->Form->input('password', ['required' => 'required']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>