<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * StatusComments Controller
 *
 * @property \App\Model\Table\StatusCommentsTable $StatusComments
 */
class StatusCommentsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $statusComments = $this->paginate($this->StatusComments);

        $this->set(compact('statusComments'));
        $this->set('_serialize', ['statusComments']);
    }

    /**
     * View method
     *
     * @param string|null $id Status Comment id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $statusComment = $this->StatusComments->get($id, [
            'contain' => ['Comments']
        ]);

        $this->set('statusComment', $statusComment);
        $this->set('_serialize', ['statusComment']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $statusComment = $this->StatusComments->newEntity();
        if ($this->request->is('post')) {
            $statusComment = $this->StatusComments->patchEntity($statusComment, $this->request->data);
            if ($this->StatusComments->save($statusComment)) {
                $this->Flash->success(__('The status comment has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The status comment could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('statusComment'));
        $this->set('_serialize', ['statusComment']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Status Comment id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $statusComment = $this->StatusComments->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $statusComment = $this->StatusComments->patchEntity($statusComment, $this->request->data);
            if ($this->StatusComments->save($statusComment)) {
                $this->Flash->success(__('The status comment has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The status comment could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('statusComment'));
        $this->set('_serialize', ['statusComment']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Status Comment id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $statusComment = $this->StatusComments->get($id);
        if ($this->StatusComments->delete($statusComment)) {
            $this->Flash->success(__('The status comment has been deleted.'));
        } else {
            $this->Flash->error(__('The status comment could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
