<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Articles Controller
 *
 * @property \App\Model\Table\ArticlesTable $Articles
 */
class ArticlesController extends AppController
{
    /**/
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['index', 'view']);
        $this->loadComponent('User');
    }


    /**/
    public function isAuthorized($user)
    {
        // use our custom component to check if user has a specific role
        if( $this->User->hasRoles(['blogger']) )
        {
            return true;
        }

        return parent::isAuthorized($user);
    }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['users', 'StatusArticles']
        ];
        $articles = $this->paginate($this->Articles);

        $this->set(compact('articles'));
        $this->set('_serialize', ['articles']);
    }


    /**
     * View method
     *
     * @param string|null $id Article id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        /* add new comment if requested */
        $i_user_id = 0;
        $this->loadModel('Comments');
        $comment = $this->Comments->newEntity();
        if ($this->request->is('post')) 
        {
            if( isset($this->request->data['body']) && !empty($this->request->data['body']) )
            {
                $s_new_comment = $this->request->data['body'];
                if(     isset($_SESSION['Auth']['User']) && !empty($_SESSION['Auth']['User']) 
                    && !empty($_SESSION['Auth']['User']['id']) 
                )
                {
                    $i_user_id = $_SESSION['Auth']['User']['id'];
                    $comment = $this->Comments->patchEntity($comment, $this->request->data);
                    $comment->body = $s_new_comment;
                    $comment->user_id = $i_user_id;
                    $comment->article_id = $id;
                    $comment->status_comment_id = 1;

                    /*print_r($comment);
                    exit();*/

                    if ($this->Comments->save($comment)) {
                        $this->Flash->success(__('The comment has been saved.'));
                    } else {
                        $this->Flash->error(__('The comment could not be saved. Please, try again.'));
                    }
                }
                else
                {
                    $this->Flash->error(__('Please login first'));
                    return $this->redirect(['controller' => 'Users', 'action' => 'logout']);
                }
            }
        }

        //
        $article = $this->Articles->get($id, [
            'contain' => ['users', 'StatusArticles', 'Categories', 'Tags', 'Comments']
        ]);

        $this->set('article', $article);
        $this->set('_serialize', ['article']);
    }


    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $i_user_id = 0;
        if( isset($_SESSION['Auth']['User']['id']) && !empty($_SESSION['Auth']['User']['id']) )
        {
            $i_user_id = $_SESSION['Auth']['User']['id'];
        }
        else
        {
            $this->Flash->error(__('Please login first'));
            return $this->redirect(['controller' => 'Users', 'action' => 'logout']);
        }

        $article = $this->Articles->newEntity();
        if ($this->request->is('post')) {
            $article = $this->Articles->patchEntity($article, $this->request->data);
            $s_slug = $this->request->data['title'];
            // replace non letter or digits by -
            $s_slug = preg_replace('~[^\\pL\d]+~u', '-', $s_slug);
            // trim
            $s_slug = trim($s_slug, '-');
            // transliterate
            $s_slug = iconv('utf-8', 'us-ascii//TRANSLIT', $s_slug);
            // lowercase
            $s_slug = strtolower($s_slug);
            // remove unwanted characters
            $s_slug = preg_replace('~[^-\w]+~', '', $s_slug);
            $article->slug = $s_slug;
            $article->user_id = $i_user_id;
            if ($this->Articles->save($article)) {
                $this->Flash->success(__('The article has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The article could not be saved. Please, try again.'));
            }
        }
        $users = $this->Articles->Users->find('list', ['limit' => 200]);
        $statusArticles = $this->Articles->StatusArticles->find('list', ['limit' => 200]);
        $categories = $this->Articles->Categories->find('list', ['limit' => 200]);
        $tags = $this->Articles->Tags->find('list', ['limit' => 200]);
        $this->set(compact('article', 'users', 'statusArticles', 'categories', 'tags'));
        $this->set('_serialize', ['article']);
    }


    /**
     * Edit method
     *
     * @param string|null $id Article id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $article = $this->Articles->get($id, [
            'contain' => ['Categories', 'Tags']
        ]);

        /* check if current user is the owner */
        $i_current_session_user = $_SESSION['Auth']['User']['id'];
        if( $article->user_id != $i_current_session_user )
        {
            // use our custom component to check if user has a specific role
            if( !$this->User->hasRoles(['admin']) )
            {
                $this->Flash->error(__('You are not the owner of this article.'));
                return $this->redirect(['action' => 'index']);
            }
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $article = $this->Articles->patchEntity($article, $this->request->data);
            if ($this->Articles->save($article)) {
                $this->Flash->success(__('The article has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The article could not be saved. Please, try again.'));
            }
        }
        $users = $this->Articles->Users->find('list', ['limit' => 200]);
        $statusArticles = $this->Articles->StatusArticles->find('list', ['limit' => 200]);
        $categories = $this->Articles->Categories->find('list', ['limit' => 200]);
        $tags = $this->Articles->Tags->find('list', ['limit' => 200]);
        $this->set(compact('article', 'users', 'statusArticles', 'categories', 'tags'));
        $this->set('_serialize', ['article']);
    }


    /**
     * Delete method
     *
     * @param string|null $id Article id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $article = $this->Articles->get($id);

        /* check if current user is the owner */
        $i_current_session_user = $_SESSION['Auth']['User']['id'];
        if( $article->user_id != $i_current_session_user )
        {
            // use our custom component to check if user has a specific role
            if( !$this->User->hasRoles(['admin']) )
            {
                $this->Flash->error(__('You are not the owner of this article.'));
                return $this->redirect(['action' => 'index']);
            }
        }

        if ($this->Articles->delete($article)) {
            $this->Flash->success(__('The article has been deleted.'));
        } else {
            $this->Flash->error(__('The article could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
