<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('cake.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <nav class="top-bar expanded" data-topbar role="navigation">
        <ul class="title-area large-3 medium-4 columns">
            <li class="name">
                <h1><a href=""><?= $this->fetch('title') ?></a></h1>
            </li>
        </ul>
        <div class="top-bar-section">
            <ul class="right">
                    <li>
                        <?= $this->Html->link(__('Home'), ['controller' => 'Articles', 'action' => 'index']) ?>
                    </li>
                <?php 
                    if( isset($a_user_session) && !empty($a_user_session) ):
                ?>
                    <li>
                        <a href="#">
                           hello, <?php echo $a_user_session['username'] ?>
                        </a>
                    </li>
                    <?php 
                        if( isset($a_user_session['a_roles_flat']) && !empty($a_user_session['a_roles_flat']) 
                            && ( in_array('admin', $a_user_session['a_roles_flat']) )
                        ):
                    ?>
                    <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
                    <li><?= $this->Html->link(__('List Status Users'), ['controller' => 'StatusUsers', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('New Status User'), ['controller' => 'StatusUsers', 'action' => 'add']) ?></li>
                    <li><?= $this->Html->link(__('List Roles'), ['controller' => 'Roles', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('New Role'), ['controller' => 'Roles', 'action' => 'add']) ?></li>
                    <?php 
                        endif;
                    ?>
                    <li>
                        <?= $this->Html->link(__('Logout'), ['controller' => 'Users', 'action' => 'logout']) ?>
                    </li>
                <?php 
                    else:
                ?>
                    <li>
                        <?= $this->Html->link(__('Login'), ['controller' => 'Users', 'action' => 'login']) ?>
                    </li>
                    <li>
                        <?= $this->Html->link(__('Register'), ['controller' => 'Users', 'action' => 'register']) ?>
                    </li>
                <?php 
                    endif;
                ?>
            </ul>

        </div>
    </nav>
    <?= $this->Flash->render() ?>
    <?= $this->Flash->render('auth') ?>
    <div class="container clearfix">
        <?= $this->fetch('content') ?>
    </div>
    <footer>
    </footer>
</body>
</html>
