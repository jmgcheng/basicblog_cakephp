<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Status User'), ['action' => 'edit', $statusUser->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Status User'), ['action' => 'delete', $statusUser->id], ['confirm' => __('Are you sure you want to delete # {0}?', $statusUser->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Status Users'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Status User'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="statusUsers view large-9 medium-8 columns content">
    <h3><?= h($statusUser->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($statusUser->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($statusUser->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Users') ?></h4>
        <?php if (!empty($statusUser->users)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th><?= __('Username') ?></th>
                <th><?= __('Email') ?></th>
                <!-- <th><?= __('Password') ?></th> -->
                <th><?= __('Firstname') ?></th>
                <th><?= __('Lastname') ?></th>
                <!-- <th><?= __('Unique Key') ?></th> -->
                <th><?= __('Status User Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($statusUser->users as $users): ?>
            <tr>
                <td><?= h($users->id) ?></td>
                <td><?= h($users->created) ?></td>
                <td><?= h($users->modified) ?></td>
                <td><?= h($users->username) ?></td>
                <td><?= h($users->email) ?></td>
                <!-- <td><?= h($users->password) ?></td> -->
                <td><?= h($users->firstname) ?></td>
                <td><?= h($users->lastname) ?></td>
                <!-- <td><?= h($users->unique_key) ?></td> -->
                <td><?= h($users->status_user_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Users', 'action' => 'view', $users->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Users', 'action' => 'edit', $users->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Users', 'action' => 'delete', $users->id], ['confirm' => __('Are you sure you want to delete # {0}?', $users->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
