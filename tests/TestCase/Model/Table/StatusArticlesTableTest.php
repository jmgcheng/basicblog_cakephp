<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StatusArticlesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StatusArticlesTable Test Case
 */
class StatusArticlesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StatusArticlesTable
     */
    public $StatusArticles;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.status_articles',
        'app.articles'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('StatusArticles') ? [] : ['className' => 'App\Model\Table\StatusArticlesTable'];
        $this->StatusArticles = TableRegistry::get('StatusArticles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->StatusArticles);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
