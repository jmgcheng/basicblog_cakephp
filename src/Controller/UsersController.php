<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Utility\Security;
use Cake\Mailer\Email;
use Cake\Mailer\MailerAwareTrait;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
    use MailerAwareTrait;

    /**/
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['index', 'logout', 'register', 'registerActivation', 'forgotPassword', 'resetPassword']);
    }


    /**/
    public function isAuthorized($user)
    {
        /*$action = $this->request->params['action'];
        // The add and index actions are always allowed.
        if (in_array($action, ['index', 'register'])) {
            return true;
        }*/
        /*// All other actions require an id.
        if (empty($this->request->params['pass'][0])) {
            return false;
        }*/

        return parent::isAuthorized($user);
    } 


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['StatusUsers']
        ];
        $users = $this->paginate($this->Users);

        //$test = $this->Users->find('all')->contain(['Roles']);
        //$this->set(compact('users', 'test'));
        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }


    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['StatusUsers', 'Roles', 'Articles', 'Comments']
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }


    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            $user->unique_key = Security::hash( strtotime('now') . $this->request->data('username') );
            
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                $o_account_activation_mailer = $this->getMailer('User');

                $s_activation_url = '';
                $a_email_params = array();
                $s_activation_token_crypt = Security::encrypt($user->unique_key, Security::salt());
                $s_activation_token_crypt = base64_encode($s_activation_token_crypt);
                $s_activation_token_crypt = strtr($s_activation_token_crypt, '+/=', '-_,');

                $s_activation_url = Router::url(['controller' => 'user', 'action' => 'register-activate'], true);
                $s_activation_url = $s_activation_url . '/' . $s_activation_token_crypt;
                $a_email_params['s_activation_token_crypt'] = $s_activation_token_crypt;
                $a_email_params['s_activation_url'] = $s_activation_url;

                /*echo $s_activation_url;
                exit();*/

                if( $o_account_activation_mailer->send('userActivation', [$user, $a_email_params]) )
                {
                    //$this->Flash->success(__('Please check your email for Account Activation'));
                    $this->Flash->success(__('Email Activation sent to the new user'));
                }
                else
                {
                    $this->Flash->success(__('mailTest not sent')); 
                }


                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $statusUsers = $this->Users->StatusUsers->find('list', ['limit' => 200]);
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $this->set(compact('user', 'statusUsers', 'roles'));
        $this->set('_serialize', ['user']);
    }


    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Roles']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $statusUsers = $this->Users->StatusUsers->find('list', ['limit' => 200]);
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $this->set(compact('user', 'statusUsers', 'roles'));
        $this->set('_serialize', ['user']);
    }


    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }


    /**/
    public function login()
    {
        //redirect to homepage if already have session
        if( isset($_SESSION['Auth']['User']) && !empty($_SESSION['Auth']['User']) )
        {
            return $this->redirect(['action' => 'index']);
        }

        if ($this->request->is('post')) 
        {
            $user = $this->Auth->identify(); //<-identify the user using credentials provided in request

            if($user) 
            {

                //get StatusUsers. Check if account is activated
                $StatusUsers = TableRegistry::get('StatusUsers');
                $o_status_user = $StatusUsers->find();
                $o_status_user->where(['id' => $user['status_user_id']]);
                $o_status_user_result = $o_status_user->first();
                if( !isset($o_status_user_result->name) || empty($o_status_user_result->name) )
                {
                    $this->Flash->error('User Status not found.');
                }
                else
                {
                    if( $o_status_user_result->name == 'activated' )
                    {
                        //check if need to setup autologin
                        if( isset($this->request->data['autologin']) && !empty($this->request->data['autologin']) && $this->request->data['autologin'] == 'remember' )
                        {
                            /*$this->Autologin->set($user['unique_key']);*/
                            $this->Cookie->write('sitename_autologin', $user['unique_key']);
                        }
                        /*echo $this->Cookie->read('sitename_autologin');
                        exit();*/


                        //
                        $this->Auth->setUser($user); //<- save user info to session
                        return $this->redirect($this->Auth->redirectUrl());
                    }
                    else
                    {
                        $this->Flash->error('Account not activated');   
                    }
                }
            }
            else
            {
                $this->Flash->error('Your username or password is incorrect.');
            }
        }
    }


    /**/
    public function logout()
    {
        $this->Cookie->delete('sitename_autologin');
        $this->Flash->success('You are now logged out.');
        return $this->redirect($this->Auth->logout());
    }



    /**/
    public function register()
    {
        //redirect to logout if already have session
        if( isset($_SESSION['Auth']['User']) && !empty($_SESSION['Auth']['User']) )
        {
            return $this->redirect(['action' => 'logout']);
        }
        
        //   
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            $user->unique_key = Security::hash( strtotime('now') . $this->request->data('username') );
            $user->status_user_id = 4; //email. should change this someday to register activation?
            
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                $o_account_activation_mailer = $this->getMailer('User');

                $s_activation_url = '';
                $a_email_params = array();
                $s_activation_token_crypt = Security::encrypt($user->unique_key, Security::salt());
                $s_activation_token_crypt = base64_encode($s_activation_token_crypt);
                $s_activation_token_crypt = strtr($s_activation_token_crypt, '+/=', '-_,');

                $s_activation_url = Router::url(['controller' => 'user', 'action' => 'register-activate'], true);
                $s_activation_url = $s_activation_url . '/' . $s_activation_token_crypt;
                $a_email_params['s_activation_token_crypt'] = $s_activation_token_crypt;
                $a_email_params['s_activation_url'] = $s_activation_url;

                /*echo $s_activation_url;
                exit();*/

                if( $o_account_activation_mailer->send('userActivation', [$user, $a_email_params]) )
                {
                    $this->Flash->success(__('Please check your email for Account Activation'));
                }
                else
                {
                    $this->Flash->success(__('mailTest not sent')); 
                }


                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }


    /**/
    public function registerActivation( $s_activation_token_crypt='' )
    {
        if( !isset($s_activation_token_crypt) || empty($s_activation_token_crypt) )
        {
            return $this->redirect(['action' => 'index']);
        }

        $s_user_activation_key = '';
        $s_activation_token_crypt = strtr($s_activation_token_crypt, '-_,', '+/=');
        $s_activation_token_crypt = base64_decode($s_activation_token_crypt);
        $s_user_activation_key = Security::decrypt($s_activation_token_crypt, Security::salt());

        if( !isset($s_user_activation_key) || empty($s_user_activation_key) )
        {
            $this->Flash->success(__('Token Invalid'));
            return $this->redirect(['action' => 'index']);
        }

        $i_user_found = 0;
        $o_user = $this->Users->find();
        $o_user->contain(['StatusUsers']);
        $o_user->where(['unique_key' => $s_user_activation_key]);
        $o_user->where(['StatusUsers.name' => 'email']);
        $o_user->limit(1);
        $i_user_found = $o_user->count();
        
        if( $i_user_found == 0 )
        {
            $this->Flash->success(__('No user found to register activate.'));
            return $this->redirect(['action' => 'index']);
        }
        elseif( $i_user_found == 1 )
        {
            $o_user = $this->Users->query();
            $o_user->update()
            ->set(['status_user_id' => 1])
            ->where(['unique_key' => $s_user_activation_key])
            ->execute();

            $this->Flash->success(__('Register Activation Successful'));
            return $this->redirect(['action' => 'index']);
        }
        else
        {
            $this->Flash->success(__('Nothing happened'));
            return $this->redirect(['action' => 'index']);
        }
    }


    /**/
    public function forgotPassword()
    {

        if ($this->request->is('post')) 
        {
            //check if post email has value
            $s_email = $this->request->data('email');
            if( !isset($s_email) || empty($s_email) )
            {
                $this->Flash->success(__('Email Address Invalid'));
            }
            else
            {
                $o_user = $this->Users->find();
                $o_user->where(['email' => $s_email]);
                $o_user_result = $o_user->first();
                /*print_r($o_user_result);*/

                if( !isset($o_user_result->email) || empty($o_user_result->email) )
                {
                    $this->Flash->success(__('User Not Found'));
                }
                else
                {
                    //echo $o_user_result->email;
                    
                    $o_user_mailer = $this->getMailer('User');

                    $s_reset_pass_request_url = '';
                    $a_email_params = array();
                    $s_token_crypt = Security::encrypt($o_user_result->unique_key, Security::salt());
                    $s_token_crypt = base64_encode($s_token_crypt);
                    $s_token_crypt = strtr($s_token_crypt, '+/=', '-_,');

                    $s_reset_pass_request_url = Router::url(['controller' => 'user', 'action' => 'reset-password'], true);
                    $s_reset_pass_request_url = $s_reset_pass_request_url . '/' . $s_token_crypt;
                    $a_email_params['s_reset_pass_request_url'] = $s_reset_pass_request_url;

                    /*echo $s_reset_pass_request_url;
                    exit();*/

                    if( $o_user_mailer->send('forgotPassword', [$o_user_result, $a_email_params]) )
                    {
                        $this->Flash->success(__('Please check your email for the request reset password of your account.'));
                    }
                    else
                    {
                        $this->Flash->success(__('mailTest not sent')); 
                    }

                }
            }
        }
    }


    /**/
    public function resetPassword( $s_token_crypt='' )
    {
        if( !isset($s_token_crypt) || empty($s_token_crypt) )
        {
            return $this->redirect(['action' => 'index']);
        }

        $s_user_key = '';
        $s_token_crypt = strtr($s_token_crypt, '-_,', '+/=');
        $s_token_crypt = base64_decode($s_token_crypt);
        $s_user_key = Security::decrypt($s_token_crypt, Security::salt());

        if( !isset($s_user_key) || empty($s_user_key) )
        {
            $this->Flash->success(__('Token Invalid'));
            return $this->redirect(['action' => 'index']);
        }
        else
        {
            $o_user = $this->Users->find();
            $o_user->select(['id', 'email']);
            $o_user->where(['unique_key' => $s_user_key]);
            $o_user_result = $o_user->first();

            if( !isset($o_user_result->email) || empty($o_user_result->email) )
            {
                $this->Flash->success(__('User Not Found'));
            }
            else
            {
                $this->set('email', $o_user_result->email);
            }
        }

        if($this->request->is('post'))
        {
            $user = $this->Users->patchEntity($o_user_result, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been updated.'));
                return $this->redirect(['action' => 'login']);
            } else {
                $this->Flash->error(__('The user could not be updated. Please, try again.'));
            }
        }
    }
    
}
