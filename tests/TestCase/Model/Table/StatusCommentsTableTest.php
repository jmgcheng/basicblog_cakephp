<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StatusCommentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StatusCommentsTable Test Case
 */
class StatusCommentsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StatusCommentsTable
     */
    public $StatusComments;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.status_comments',
        'app.comments'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('StatusComments') ? [] : ['className' => 'App\Model\Table\StatusCommentsTable'];
        $this->StatusComments = TableRegistry::get('StatusComments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->StatusComments);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
