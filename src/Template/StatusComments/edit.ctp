<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $statusComment->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $statusComment->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Status Comments'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Comments'), ['controller' => 'Comments', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Comment'), ['controller' => 'Comments', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="statusComments form large-9 medium-8 columns content">
    <?= $this->Form->create($statusComment) ?>
    <fieldset>
        <legend><?= __('Edit Status Comment') ?></legend>
        <?php
            echo $this->Form->input('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
