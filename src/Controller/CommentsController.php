<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Comments Controller
 *
 * @property \App\Model\Table\CommentsTable $Comments
 */
class CommentsController extends AppController
{
    /**/
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('User');
    }


    /**/
    public function isAuthorized($user)
    {
        // use our custom component to check if user has a specific role
        if( $this->User->hasRoles(['blogger']) )
        {
            return true;
        }

        return parent::isAuthorized($user);
    }


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Articles', 'users', 'StatusComments']
        ];
        $comments = $this->paginate($this->Comments);

        $this->set(compact('comments'));
        $this->set('_serialize', ['comments']);
    }

    /**
     * View method
     *
     * @param string|null $id Comment id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $comment = $this->Comments->get($id, [
            'contain' => ['Articles', 'users', 'StatusComments']
        ]);

        $this->set('comment', $comment);
        $this->set('_serialize', ['comment']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $comment = $this->Comments->newEntity();
        if ($this->request->is('post')) {
            $comment = $this->Comments->patchEntity($comment, $this->request->data);
            if ($this->Comments->save($comment)) {
                $this->Flash->success(__('The comment has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The comment could not be saved. Please, try again.'));
            }
        }
        $articles = $this->Comments->Articles->find('list', ['limit' => 200]);
        $users = $this->Comments->Users->find('list', ['limit' => 200]);
        $statusComments = $this->Comments->StatusComments->find('list', ['limit' => 200]);
        $this->set(compact('comment', 'articles', 'users', 'statusComments'));
        $this->set('_serialize', ['comment']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Comment id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $comment = $this->Comments->get($id, [
            'contain' => []
        ]);

        /* check if current user is the owner */
        $i_current_session_user = $_SESSION['Auth']['User']['id'];
        if( $comment->user_id != $i_current_session_user )
        {
            // use our custom component to check if user has a specific role
            if( !$this->User->hasRoles(['admin']) )
            {
                $this->Flash->error(__('You are not the owner of this article.'));
                return $this->redirect(['action' => 'index']);
            }
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $comment = $this->Comments->patchEntity($comment, $this->request->data);
            if ($this->Comments->save($comment)) {
                $this->Flash->success(__('The comment has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The comment could not be saved. Please, try again.'));
            }
        }
        $articles = $this->Comments->Articles->find('list', ['limit' => 200]);
        $users = $this->Comments->Users->find('list', ['limit' => 200]);
        $statusComments = $this->Comments->StatusComments->find('list', ['limit' => 200]);
        $this->set(compact('comment', 'articles', 'users', 'statusComments'));
        $this->set('_serialize', ['comment']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Comment id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $comment = $this->Comments->get($id);

        /* check if current user is the owner */
        $i_current_session_user = $_SESSION['Auth']['User']['id'];
        if( $comment->user_id != $i_current_session_user )
        {
            // use our custom component to check if user has a specific role
            if( !$this->User->hasRoles(['admin']) )
            {
                $this->Flash->error(__('You are not the owner of this article.'));
                return $this->redirect(['action' => 'index']);
            }
        }
        
        if ($this->Comments->delete($comment)) {
            $this->Flash->success(__('The comment has been deleted.'));
        } else {
            $this->Flash->error(__('The comment could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
