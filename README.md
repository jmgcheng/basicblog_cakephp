# CakePHP Application - Basic Blog Skeleton

Cake skeleton with Basic Blog.

## Tasks To Do:
* theme
* 
* -


## App Capabilities:
* users - has [bum_cakephp](https://bitbucket.org/jmgcheng/bum_cakephp) integrated
* blog - add articles - specific roles
* blog - list articles
* blog - view article
* blog - edit articles - edit, draft, trash articles - specific users
* blog - delete articles - specific users
* blog - create article categories - specific roles
* blog - list article categories
* blog - view article categories
* blog - edit article categories - specific roles
* blog - delete article categories - specific roles
* blog - create article tags - specific roles
* blog - list article tags
* blog - view article tags
* blog - edit article tags - specific roles
* blog - delete article tags - specific roles
* blog - create article comments - login users
* blog - edit article comments - edit, delete article comments - specific users
* blog - delete article comments - specific users
* -


## Current DB:
```
#!sql

CREATE TABLE status_users
(
	id INT(11) AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(55) NOT NULL
);
CREATE TABLE users 
(
	id INT(11) AUTO_INCREMENT PRIMARY KEY,
	created DATETIME,
	modified DATETIME,
	username VARCHAR(55) NOT NULL,
	email VARCHAR(55) NOT NULL,
	password VARCHAR(255) NOT NULL,
	firstname VARCHAR(55) DEFAULT '',
	lastname VARCHAR(55) DEFAULT '',
	unique_key VARCHAR(255) NOT NULL,
	status_user_id int(11) DEFAULT 1,
	FOREIGN KEY status_user_key (status_user_id) REFERENCES status_users(id)
);
CREATE TABLE roles
(
	id INT(11) AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(55) NOT NULL,
	UNIQUE KEY (name)
);
CREATE TABLE roles_users
(
	user_id INT(11) NOT NULL,
	role_id INT(11) NOT NULL,
	PRIMARY KEY (user_id, role_id),
    FOREIGN KEY user_key (user_id) REFERENCES users(id),
    FOREIGN KEY role_key (role_id) REFERENCES roles(id)
);

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'Admin'),
(2, 'User'),
(3, 'Blogger');
INSERT INTO `status_users` (`id`, `name`) VALUES
(1, 'activated'),
(2, 'banned'),
(3, 'deactivated'),
(4, 'email'),
(5, 'lock');

CREATE TABLE status_articles
(
	id INT(11) AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(55) NOT NULL
);
CREATE TABLE articles 
(
    id INT(11) AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(100) NOT NULL,
    slug VARCHAR(150) NOT NULL,
    body TEXT NOT NULL,
    created DATETIME DEFAULT NULL,
    modified DATETIME DEFAULT NULL,
    user_id INT(11) NOT NULL,
    status_article_id int(11) NOT NULL
);
CREATE TABLE categories 
(
	id INT(11) AUTO_INCREMENT PRIMARY KEY,
	parent_id INT(11) NOT NULL,
	lft INT(10) NOT NULL,
	rght INT(10) NOT NULL,
	name VARCHAR(50) NOT NULL,
	description VARCHAR(255)
);
CREATE TABLE articles_categories
(
	article_id INT(11) NOT NULL,
	category_id INT(11) NOT NULL,
	PRIMARY KEY (article_id, category_id),
    FOREIGN KEY article_key (article_id) REFERENCES articles(id),
    FOREIGN KEY category_key (category_id) REFERENCES categories(id)
);
CREATE TABLE tags 
(
    id INT(11) AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50)
);
CREATE TABLE articles_tags
(
	article_id INT(11) NOT NULL,
	tag_id INT(11) NOT NULL,
	PRIMARY KEY (article_id, tag_id),
    FOREIGN KEY article_key (article_id) REFERENCES articles(id),
    FOREIGN KEY category_key (tag_id) REFERENCES tags(id)
);

INSERT INTO `categories` (`id`, `parent_id`, `lft`, `rght`, `name`, `description`) VALUES
(1, 0, 1, 8, 'Category 1 - lvl 1', ''),
(2, 1, 2, 5, 'Category 2 - lvl 1 2', ''),
(3, 1, 6, 7, 'Category 3 - lvl 1 2', ''),
(4, 2, 3, 4, 'Category 4 - lvl 2 3', '');
INSERT INTO `tags` (`id`, `name`) VALUES
(1, 'Tag 1'),
(2, 'Tag 2'),
(3, 'Tag 3');
INSERT INTO `status_articles` (`id`, `name`) VALUES
(1, 'Published'),
(2, 'Trashed'),
(3, 'Draft');

CREATE TABLE status_comments
(
	id INT(11) AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(55) NOT NULL
);
CREATE TABLE comments
(
	id INT(11) AUTO_INCREMENT PRIMARY KEY,
    body TEXT NOT NULL,
    created DATETIME DEFAULT NULL,
    modified DATETIME DEFAULT NULL,
    article_id INT(11) NOT NULL,
    user_id INT(11) NOT NULL,
    status_comment_id int(11) NOT NULL
);
INSERT INTO `status_comments` (`id`, `name`) VALUES
(1, 'Published'),
(2, 'Trashed');


```

--------------------------------------------------


## Setup:



--------------------------------------------------


## Setup - Fresh:
1. Download [Composer](http://getcomposer.org/doc/00-intro.md) or update `composer self-update`.
2. Run `php composer.phar create-project --prefer-dist cakephp/app [app_name]`.
If Composer is installed globally, run
```bash
composer create-project --prefer-dist cakephp/app [app_name]
```
3. Run browser and check if installation is ok
4. Create DB. Use schema provided
5. Read and edit `config/app.php` and setup the 'Datasources' and any other configuration relevant for your application.
6. Check if able to connect DB
7. Bake
```bash
bin/cake bake all
bin/cake bake all roles
bin/cake bake all status_users
bin/cake bake all users
bin/cake bake all tags
bin/cake bake all categories
bin/cake bake all status_articles
bin/cake bake all articles
bin/cake bake all status_comments
bin/cake bake all comments

```

--------------------------------------------------


## Notes:
* -