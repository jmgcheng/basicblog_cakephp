<?php 
namespace App\Controller\Component;

use Cake\Controller\Component;

class UserComponent extends Component
{
    public function hasRoles($a_roles = array())
    {
        $a_user_session_roles = array();
        if( isset($_SESSION['Auth']['User']['a_roles_flat']) && !empty($_SESSION['Auth']['User']['a_roles_flat']) )
        {
            $a_user_session_roles = $_SESSION['Auth']['User']['a_roles_flat'];
        }

        if( !empty($a_user_session_roles) && !empty($a_roles) )
        {
            foreach ($a_roles AS $s_role) 
            {
                if(in_array($s_role, $a_user_session_roles)) 
                {
                    return true;
                }
            }   
        }

        return false;
    }




}