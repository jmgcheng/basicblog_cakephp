<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * StatusUsers Controller
 *
 * @property \App\Model\Table\StatusUsersTable $StatusUsers
 */
class StatusUsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $statusUsers = $this->paginate($this->StatusUsers);

        $this->set(compact('statusUsers'));
        $this->set('_serialize', ['statusUsers']);
    }

    /**
     * View method
     *
     * @param string|null $id Status User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $statusUser = $this->StatusUsers->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('statusUser', $statusUser);
        $this->set('_serialize', ['statusUser']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $statusUser = $this->StatusUsers->newEntity();
        if ($this->request->is('post')) {
            $statusUser = $this->StatusUsers->patchEntity($statusUser, $this->request->data);
            if ($this->StatusUsers->save($statusUser)) {
                $this->Flash->success(__('The status user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The status user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('statusUser'));
        $this->set('_serialize', ['statusUser']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Status User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $statusUser = $this->StatusUsers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $statusUser = $this->StatusUsers->patchEntity($statusUser, $this->request->data);
            if ($this->StatusUsers->save($statusUser)) {
                $this->Flash->success(__('The status user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The status user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('statusUser'));
        $this->set('_serialize', ['statusUser']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Status User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $statusUser = $this->StatusUsers->get($id);
        if ($this->StatusUsers->delete($statusUser)) {
            $this->Flash->success(__('The status user has been deleted.'));
        } else {
            $this->Flash->error(__('The status user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
