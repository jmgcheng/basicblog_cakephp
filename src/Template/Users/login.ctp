<nav class="large-3 medium-4 columns" id="actions-sidebar">
</nav>
<div class="users form large-9 medium-8 columns content">
	<h1>Login</h1>
	<?= $this->Form->create() ?>
	<?= $this->Form->input('username') ?>
	<?= $this->Form->input('password') ?>
	<input type="checkbox" name="autologin" value="remember" /> Autologin <br/>
	<?= $this->Form->button('Login') ?>
	<?= $this->Form->end() ?>
	<p>
		<?php 
			echo $this->Html->link(
			    'Forgot Password?',
			    ['controller' => 'user', 'action' => 'forgot-password']
			);
		?>
	</p>
</div>
